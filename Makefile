# use this to disable flto optimizations:
#   make NO_FLTO=1
# and this to enable verbose mode:
#   make V=1

ifndef NO_FLTO
CFLAGS = -O3 -fno-stack-protector -flto -Wall -Wextra -std=gnu90
LDFLAGS	= -O3 -fno-stack-protector -flto
else
CFLAGS = -O3 -fno-stack-protector -Wall -Wextra -std=gnu90
LDFLAGS	= -O3 -fno-stack-protector
endif

OBJDIR = .
SRCDIR = src
SOURCES  := $(wildcard $(SRCDIR)/*.c)

OBJS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

all: eoip

eoip: $(OBJS)
	@echo " [eoip] CC $@"
	@$(CC) -o $@ $(OBJS)

	$(SIZECHECK)
	$(CPTMP)


install:
	@echo " [eoip] Installing to $(INSTALLDIR)"
	@install -D eoip $(INSTALLDIR)/usr/bin/eoip
	@$(STRIP) $(INSTALLDIR)/usr/bin/eoip
	@chmod 0755 $(INSTALLDIR)/usr/bin/eoip

clean:
	@echo " no clean"

size: eoip
	mipsel-uclibc-nm --print-size --size-sort eoip

$(OBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@echo " [eoip] CC $@"
	@$(CC) $(CFLAGS) -c $< -o $@
